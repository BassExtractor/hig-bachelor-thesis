# README #

This is the template for both the Bachelor and Masters thesis in Latex for NTNU.

### What was this repository for? ###

* Fork the repository to your own project and then you can start editing it and adding your own content
* The key files are
** ntnuthesis.cls
** ntnuthesis.bst
* You should read the other files and document as it tells you quite a bit about how to use the documents

### How did I get set up? ###

* Fork to your own repository on Bitbucket, Github, or similar
* Clone your repo onto your local machine
* Edit content and do standard git workflow
** pull repo
** edit content
** commit changes
** when ready to share push to remote repo
* Collaborate with the rest of the group and be careful with merging.


### Contributions ###

* If you think there should be an update sorry, edit the documents on your version of the repo, and send a pull request to an admin, Simon, Rune, or Mariusz currently.  We may or may not update the standard template.